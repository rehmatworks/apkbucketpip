from apkbucketpip.infojson import infojson
from apkbucketpip.apkinfo import apkinfo
# Get info of an APK file in JSON form
jsondata = infojson('filename.apk')