from .apk import APK
import json

def infojson(apkfile):
    apk_path = apkfile
    apkf = APK(apk_path)
    data = {
        'certificate': apkf.cert_text,
        'package': apkf.package,
        'fileMd5': apkf.file_md5,
        'certMd5': apkf.cert_md5,
        'fileSize': apkf.file_size,
        'version': apkf.androidversion,
        'isValid': apkf.is_valid_APK(),
        'versionCode': apkf.get_androidversion_code(),
        'versionName': apkf.get_androidversion_name(),
        'maxSdk': apkf.get_max_sdk_version(),
        'minSdk': apkf.get_min_sdk_version(),
        'targetSdk': apkf.get_target_sdk_version(),
        'mainActivity': apkf.get_main_activity(),
        'activities': apkf.get_activities(),
        'services': apkf.get_services(),
        'receivers': apkf.get_receivers(),
        'providers': apkf.get_providers(),
        'permissions': apkf.get_permissions(),
        'permissionDetails': apkf.get_details_permissions(),
        'signature': apkf.get_signature_name()
    }

    return json.dumps(data)