from setuptools import setup

setup(
	name="apkbucketpip",
	version="1.0",
	packages=["apkbucketpip"],
	install_requires=[
	],
	description="A package for PIP that makes use of apk_parser Python package to read info from APK files.",
	author='Rehmat Alam',
	author_email='contact@rehmat.works',
	url='https://apkbucket.net',
)